# How to Setup a New Node

## AWS Configuration

### Create a new EC2 instance

If a similar node is already running:
 
- Go to the EC2 dashboard
- Select that instance
- Perform: Action -> Launch More Like This
- Edit the "Tags" and adjust the "Name "value"
- Select: Launch

Otherwise, do these steps:

- AMI Type: Amazon Linux 2 AMI
- Instance type:
    - typically _t2.small_
    - Manage requires _t3.medium_ for 4G memory
- Instance Details: defaults ok
- Storage:
    - typically default ok
    - Manage requires 20GB for logs and swap
- Tags:
    - Name: snapshot-PRODUCT-VERSION (such as _snapshot_manage_v461_)
- Security group:
    - For Manage, use: ECO Manage Instance
    - For a simple Spring Boot app: SSH, All ICMPv4, TCP/8080
    - Other products, enable ports as needed
- SSH key: existing key pair _mgmtsys-us-east-2_

### Create a static IP

- Return to the "EC2 Running Instances" panel
- Select the newly created EC2 instance
- Select: Actions -> Networking -> Manage IP Addresses
- Select: Allocate an Elastic IP
- Select: Allocate
- Select: Associate this Elastic IP Address
- Instance: (select name of the newly created EC2 instance)
- Select: Associate
- Edit the "Name" field for this address and enter the EC2 instance name, for instance _snapshot-paragon-v100_ 
- Copy the IP address to the clipboard

Create a DNS Entry:

- Go to: Route 53
- Select: Hosted zones -> arriseco.com -> Create record set
- Enter record information:
    - Name: name of the EC2 instance, for instance _snapshot-paragon-v100_ 
    - Value: paste the IP address copied above
    - Leave everything else as defaults
    - Select: create

At this point, the new EC2 instance should be running with a public IP address and DNS name.
Use the _ping_ command to verify.          

## Ansible configuration

### Update ansiblerepo with new node

- add <server name> to hosts file
- create <server name>.yml file in host_vars as follows:
    
        ---
        ansible_host: <FULL SERVER URL>

- go to roles/eco_manage/files and create new folder <server name>
- in the folder, add the preferred docker compose file for the node

### Use Ansible to set up manage nodes

Run ansible with:

        ansible-playbook --private-key ~/.ssh/mgmtsys-us-east-2.pem -i hosts site.yml

If you load the _mgmtsys-us-east-2.pem_ key into your keyring then you can drop the
_--private-key_ option. Otherwise, adjust the --private-key arg to the path of your copy of the _mgmtsys-us-east-2.pem_ key.

### Test the instance

The manage nodes should now be connected to the Node Status Monitor, go to 
<placeholder_url>/status to make sure they are all there.

More documentation on the Node Status Monitor can be found here:
https://arrisworkassure.atlassian.net/wiki/spaces/ECO/pages/1455620356/Node+Status+Monitor+--+ECO+Manage+Cloud+BVT+Project



