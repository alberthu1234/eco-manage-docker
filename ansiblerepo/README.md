Run with:

    ansible-playbook --private-key ~/.ssh/mgmtsys-us-east-2.pem -i hosts site.yml

If you load the _mgmtsys-us-east-2.pem_ key into your keyring then you can drop the
_--private-key_ option.

Otherwise, adjust the --private-key arg to the path of your copy of the _mgmtsys-us-east-2.pem_ key.
