### Node Status Monitor explained ###

Documentation on NSM features can be found here: 

https://arrisworkassure.atlassian.net/wiki/spaces/ECO/pages/1455620356/Node+Status+Monitor+--+ECO+Manage+Cloud+BVT+Project

To run a local development server of the NSM:

    export FLASK_APP=NSMonitor.py
    flask run
    
To instead run the server in debug mode:

    export FLASK_APP=NSMonitor.py
    export FLASK_ENV=development
    flask run
    
To send post requests to the server, use curl with the following format, replacing the host, service, and event values 
with your desired values.
The "host" key-value pair is optional, and will be replaced with the user sending the curl's ip address if unused.

    curl -d '{"host": "manage-snapshot-v461.arriseco.com", "service": "manage-devices", "event": "SERVICE-STARTED"}' -H "Content-Type: application/json" -X POST http://127.0.0.1:5000/event

To see the raw JSON data of the nodes being managed by the NSM, go to 
    
    http://127.0.0.1:5000/status/data

To see the data of the nodes in a datatable format, go to

    http://127.0.0.1:5000/status
    
To reset the nodes saved in the server, delete the NSM_data.pickle file

To see detailed logs of the NSM, go to the event.log file

    
    


