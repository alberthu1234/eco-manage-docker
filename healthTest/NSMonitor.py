import NSMonitorUtil
import TrackedInfoService
from flask import Flask, request, make_response, redirect, jsonify, render_template
import requests
import json
import logging

app = Flask(__name__)

username = "service"
password = "2wire"
FORMAT = "%(asctime)s %(levelname)s %(message)s"
DATEFORMAT = '%Y-%m-%dT%H:%M:%SZ'
logging.basicConfig(filename='event.log', level=logging.DEBUG, format=FORMAT, datefmt=DATEFORMAT)


@app.before_first_request
def startup():
	""" Load state and service test on startup """
	TrackedInfoService.loadState()
	for info in TrackedInfoService.getAllTrackedInfo():
		NSMonitorUtil.serviceTest(info)
	#congestion handling //fixme

@app.route('/', methods=['GET'])
def home():
    return redirect("/status")

@app.route('/status', methods=['GET'])
def status():
	"""
	if not verifyRequest(request.authorization):
	return make_response('Could not verify!', 401, {'WWW-Authenticate' : 'Basic realm="Login Required"'})
	"""
	return render_template("index.html")

@app.route('/status/data', methods=['GET'])
def statusData():
	"""
	if not verifyRequest(request.authorization):
		return make_response('Could not verify!', 401, {'WWW-Authenticate' : 'Basic realm="Login Required"'})
	"""
	return jsonify([t.serialize() for t in TrackedInfoService.getAllTrackedInfo()])

@app.route('/event', methods=['POST'])
def event():
	"""
	if not verifyRequest(request.authorization):
		return make_response('Could not verify!', 401, {'WWW-Authenticate' : 'Basic realm="Login Required"'})
	"""
	data = request.get_json()
	service = data['service']
	event = data['event']
	if 'host' in data.keys():
		host = data['host']
	else:
		host = request.remote_addr
	logging.info("Event recieved: host " + host + ", service " + service + ", event " + event)
	info = TrackedInfoService.getTrackedInfo(host, service)
	NSMonitorUtil.handleEvent(info, event)
	TrackedInfoService.saveState()
	return '/event response completed'

def verifyRequest(auth):
	""" HTTP authentication of user """
	return auth and auth.username == username and auth.password == password

if __name__ == "__main__":
    application.run(host='0.0.0.0', port='80')

