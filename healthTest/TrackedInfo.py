from Common import TestStatus

MANAGE_DEVICES = "manage-devices"
MANAGE_SERVICES = "manage-services"

class TrackedInfo:
	"""
	Encapsulates the state of a manage instance
	
	Attributes:
	host: str, host or address of instance
	service: str, options are "manage-devices" or "manage-services"
	last_event: str, options are "PRE-CHECK", "PRE-UPDATE", "POST-UPDATE", "POST-CHECK", "SERVICE-STARTED"
	test_status: str, PASS or FAIL depending on test result
	test_date: datetime object, date of most recent test
	first_fail: datetime object, date of first testing failure since last PASS
	timer: timer object, runs until expired() is called or until the next event
	startup_date: datetime object, date of last SERVICE_STARTED event
	service_url: URL of the manage service of the host
	"""

	def __init__(self, host, service):
		self.host = host
		self.service = service
		self.last_event = None
		self.test_status = TestStatus.NOT_TESTED.value
		self.test_date = None
		self.first_fail = None
		self.timer = None
		self.startup_date = None
		self.service_url = TrackedInfo.serviceURL(host, service)

	@staticmethod
	def serviceURL(host, service):
		if service == MANAGE_DEVICES:
			return "http://" + host + ":9184/manage"
		elif service == MANAGE_SERVICES:
			return "http://" + host + ":9280/manage-services"
	
	def __getstate__(self):
		d = dict(self.__dict__)
		d['timer'] = None
		return d
	
	def serialize(self):
		return {
			'host': self.host,
			'service': self.service,
			'last_event': self.last_event,
			'test_status': self.test_status,
			'test_date': self.test_date,
			'first_fail': self.first_fail,
			'startup_date': self.startup_date,
			'service_url': self.service_url
		}