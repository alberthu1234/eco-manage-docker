from enum import Enum, IntEnum

class LoggingLevel(IntEnum):
	DEBUG = 10
	INFO = 20
	WARNING = 30
	ERROR = 40
	CRITICAL = 50

class TestStatus(Enum):
	PASS = "PASS"
	FAIL = "FAIL"
	NOT_TESTED = "NOT_TESTED"