import unittest
import requests
import json
from unittest.mock import Mock
from mock import patch
import NSMonitor

TESTHOST = "testhost"
TESTEVENT = "testevent"
TESTSERVICE = "testservice"

tracked_info = {}

class NSMTest(unittest.TestCase):

	@patch("NSMonitor.slackAlert")
	def test_expired(self, mock_slack):
		mock_obj = Mock()
		mock_obj.host = TESTHOST
		mock_obj.event = TESTEVENT
		mock_obj.service = TESTSERVICE
		#log = "timer expired: host " + mock_obj.host + ", service " + mock_obj.service + ", event " + mock_obj.event
		#mock_slack.return_value = "mock works"
		NSMonitor.expired(mock_obj)
		self.assertEqual(mock_slack.call_count, 1)

	@patch("NSMonitor.slackAlert")
	def test_getTrackedInfo(self, mock_slack):
		x = NSMonitor.getTrackedInfo(TESTHOST, TESTSERVICE, TESTEVENT)
		self.assertEqual(mock_slack.call_count, 1)
		assertEqual(x, NSMonitor.TrackedInfo(TESTHOST, TESTEVENT, TESTSERVICE))

if __name__ == '__main__':
	unittest.main()

