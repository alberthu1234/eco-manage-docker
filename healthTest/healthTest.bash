#!/bin/bash

mdevicesHealthTest() {
    # Verify ECO Manage for Devices is running
    testUrl "http://${1}:9184/versionInfo" || fail "ECO Manage for Devices versioninfo not accessible"
    testUrl "http://${1}:9180/" || fail "ECO Manage CWMP not accessible"
    testUrl "http://${1}:9184/manage" || fail "ECO Manage for Devices UI not accessible"
    testUrl "http://${1}:9184/dmc-nbiws/nbi-ws/deviceManagement.wsdl" || fail "ECO Manage device NBI not accessible"
}

mservicesHealthTest() {
    # Verify ECO Manage for Services is running
    testUrl "http://${1}:9280/versioninfo" || fail "ECO Manage for Services versioninfo not accessible"
    testUrl "http://${1}:9280/manage-services" || fail "ECO Manage for Services UI not accessible"
    testUrl "http://${1}:9280/sms-ws/ws/subscriberManagement.wsdl" || fail "ECO Manage service NBI not accessible"
}


testUrl() {
    status=$(curl --write-out %{http_code} --silent --output /dev/null --connect-timeout 60 $1)
    echo "URL $1 returned HTTP status code $status"
        if [ $status == "200" -o $status == "302" -o $status == "401" ]; then
            return 0
        fi
        return 1
}

fail() {
    echo "ERROR: $1"
    if [ ! -t 0 -a -n "$EMAIL_ADDR" ]; then
        (
            echo "An error occurred while running $0:"
            echo "  $@"
            echo ""
            echo "Current environment:"
            env
        ) | mail -s "BVT Failure on `hostname`" ~r $DIST_HOME/log $EMAIL_ADDR
    fi
    exit 1
}

service=${1:-all}
host=${2:-"localhost"}

if [ $service == "dmc" ] || [ $service == "all" ]; then
    mdevicesHealthTest $host
fi
if [ $service == "sms" ] || [ $service == "all" ]; then
    mservicesHealthTest $host
fi