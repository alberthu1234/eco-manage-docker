from Common import *
from datetime import datetime, timedelta
from healthTest import testScript
from threading import Timer
from TrackedInfo import TrackedInfo
import TrackedInfoService
import logging
import json
import requests

EVENT_SERVICE_STARTED = "SERVICE-STARTED"
MAX_FAILURE_DAYS = timedelta(days=5)
WEBHOOK_URL = 'https://hooks.slack.com/services/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'

FORMAT = "%(asctime)s %(levelname)s %(message)s"
DATEFORMAT = '%Y-%m-%dT%H:%M:%SZ'
logging.basicConfig(filename='event.log', level=logging.DEBUG, format=FORMAT, datefmt=DATEFORMAT)

def handleEvent(info, event):
	"""
	Stops the timer if new event. Starts the timer waiting for the next event.
	Initiates hourly service test if SERVICE_STARTED event.
	"""
	old_event = info.last_event
	info.last_event = event
	if info.timer:
		info.timer.cancel()
		info.timer = None
	if event != EVENT_SERVICE_STARTED:
		info.timer = Timer(240.0, expired, [info])
		info.timer.start()
	else:
		info.startup_date = datetime.now()
		if old_event != None:
			log = "Existing node " + info.host + " has been restarted"
			slackAlert(log, LoggingLevel.INFO)
		serviceTest(info)


def slackAlert(log, level=LoggingLevel.INFO):
	""" Logs the information and slacks an alert """
	logging.log(level, log)
	slack_data = {"text": log}
	response = requests.post(WEBHOOK_URL, data=json.dumps(slack_data), headers={'Content-Type': 'application/json'})
	if response.status_code != 200:
		raise ValueError(
			'Request to slack returned an error %s, the response is:\n%s' % (response.status_code, response.text))
	return response


def expired(info):
	""" Function passes into the timer. Logs the timer expiring and issues a slack alert """
	log = "Timer expired: host " + info.host + ", service " + info.service + ", event " + info.last_event
	slackAlert(log, LoggingLevel.WARNING)

def serviceTest(info):
	"""
	Tests info.service, records the result, and sets a timer to repeat that test every hour.
		
	Issues an alert if there is a change from the previous test result or repeated testing failure.
	If repeated testing failure, checks if the node has failed for MAX_FAILURE_DAYS,
	and ends hourly testing for that node if so. 
	"""
	try:
		test_result = testScript(info.host, info.service)
	except ValueError as e:
		slackAlert(str(e), LoggingLevel.WARNING)
		TrackedInfoService.removeTrackedInfo(info)
		return
	log = "Test result: " + test_result + ", host " + info.host + ", service " + info.service
	logging.info(log)
	info.test_date = datetime.now()

	info.timer = Timer(3600.0, serviceTest, [info])
	info.timer.start()

	old_result = info.test_status
	if test_result != old_result:
		log = "Change in test result: " + old_result + " -> " + test_result + ", host " + info.host + ", service " + info.service
		slackAlert(log, LoggingLevel.WARNING)
		info.test_status = test_result
		if test_result == TestStatus.FAIL.value:
			info.first_fail = datetime.now()
	elif old_result == TestStatus.FAIL.value and datetime.now() - info.first_fail >= MAX_FAILURE_DAYS:
		log = "Repeated testing failure, monitoring terminated: host " + info.host + ", service " + info.service
		slackAlert(log, LoggingLevel.WARNING)
		TrackedInfoService.removeTrackedInfo(info)
	TrackedInfoService.saveState()
