#!/usr/bin/python
#takes in 2 optional arguments, service and hostname

import sys
import requests

def mdevicesHealthTest(host):
	# Verify ECO Manage for Devices is running
	if not testURL("http://" + host + ":9184/versionInfo"):
		return fail("ECO Manage for Devices versioninfo not accessible")
	if not testURL("http://" + host + ":9180/"):
		return fail("ECO Manage CWMP not accessible") 
	if not testURL("http://" + host + ":9184/manage"):
		return fail("ECO Manage for Devices UI not accessible")
	if not testURL("http://" + host + ":9184/dmc-nbiws/nbi-ws/deviceManagement.wsdl"):
		return fail("ECO Manage device NBI not accessible")
	return True

def mservicesHealthTest(host):
	if not testURL("http://" + host + ":9280/versioninfo"):
		return fail("ECO Manage for Services versioninfo not accessible")
	if not testURL("http://" + host + ":9280/manage-services"):
		return fail("ECO Manage for Services UI not accessible") 
	if not testURL("http://" + host + ":9280/sms-ws/ws/subscriberManagement.wsdl"):
		return fail("ECO Manage service NBI not accessible")
	return True

def testURL(url):
	try: 
		response = requests.get(url)
	except requests.exceptions.ConnectionError:
		return fail(url)

	print("URL " + url + " returned HTTP status code " + str(response.status_code))
	return (response.status_code == 200 or response.status_code == 302 or response.status_code == 401)

def fail(url):
	print("ERROR:" + url)
	return False

"""Script for node status monitor automated testing, returns true if all tests passed, false if not"""
def testScript(host, service):
	if service == "manage-devices":
		return boolToPassFail(mdevicesHealthTest(host))
	elif service == "manage-services":
		return boolToPassFail(mservicesHealthTest(host))
	else:
		raise ValueError('Unknown service: ' + service + ", host " + host + ' rejected')

def boolToPassFail(bool):
    return "PASS" if bool else "FAIL"


