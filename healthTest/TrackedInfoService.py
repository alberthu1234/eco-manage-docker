from Common import *
from TrackedInfo import TrackedInfo
import NSMonitorUtil
import logging
import pickle

FORMAT = "%(asctime)s %(levelname)s %(message)s"
DATEFORMAT = '%Y-%m-%dT%H:%M:%SZ'
logging.basicConfig(filename='event.log', level=logging.DEBUG, format=FORMAT, datefmt=DATEFORMAT)

STATE_FILE = 'NSM_data.pickle'

def saveState():
	""" Saves tracked_info to STATE_FILE. """
	with open(STATE_FILE, 'wb') as handle:
		pickle.dump(tracked_info, handle, protocol=pickle.HIGHEST_PROTOCOL)


def loadState():
	""" Loads tracked_info from STATE_FILE if it exists. """
	global tracked_info
	try:
		with open(STATE_FILE, 'rb') as handle:
			tracked_info = pickle.load(handle)
			logging.info("Saved state loaded from file")
	except (OSError, IOError) as e:
		tracked_info = {}

def getTrackedInfo(host, service):
	"""
	Creates a TrackedInfo object if a new node or service has been started. 
	Returns: the TrackedInfo object associated with the host and service.
	"""
	if host not in tracked_info.keys():
		tracked_info[host] = {service: TrackedInfo(host, service)}
		log = "New node " + host + " has been started"
		NSMonitorUtil.slackAlert(log, LoggingLevel.INFO)
	elif service not in tracked_info[host].keys():
		tracked_info[host][service] = TrackedInfo(host, service)
	return tracked_info[host][service]


def getAllTrackedInfo():
	""" Puts all TrackedInfo objects in tracked_info into a list """
	result = []
	for key in tracked_info.keys():
		for key, val in tracked_info[key].items():
			result.append(val)
	return result

def removeTrackedInfo(info):
	""" Removes the specified TrackedInfo object from the NSM """
	del tracked_info[info.host][info.service]
	saveState()
